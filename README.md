# TP_et_TD

# Plusieurs sujets de TP et de TD à proposer en formation. 

Il s'agit de proposer aux étudiants différents TP et TD afin de faire face à des environnements GNU/Linux de type entreprise sur systèmes CentOS/RHEL/Fedora.

Certains disposeront de solutions (private) afin de ne pas révéler les méthodes aux candidats.

1 - [Backup externalisé d'une partition présente sur une VM](https://gitlab.com/valentingrebert/tp_et_td/-/blob/master/TP_Backup_VM.md)

2 - [Administration d'un serveur Web, ssh, sftp, sur VM CentOS6](https://gitlab.com/valentingrebert/tp_et_td/-/blob/master/TP_CentOS6.md)

3 - [Installation d'une instance serveur de gitlab CE](https://gitlab.com/valentingrebert/tp_et_td/-/blob/master/TP_Gitlab.md)