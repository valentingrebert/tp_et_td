# TP sauvegarde et FTP 

Le but du TP est :

Monter deux VM au sein d'un hyperviseur de votre choix. (Systeme GNU/Linux obligatoire)

1. Vous diposez de peu d'espace et devez operer une sauvegarde d'une partition  de la VM1 vers la VM2.

2. Vous devrez effectuer la sauvegarde avec le outils de votre choix de façon securisée.

3. Vous devrez servir la sauvegarde sur un FTPS avec authentification.

4. Verifier la sauvegarde et mettre en place une routine (2 par semaines).