# TP_Backup_VM

### [Enoncé](https://gitlab.com/valentingrebert/tp_et_td/blob/master/TP_Backup_VM.md)

--- ---

# Prérequis sur la structure :

* Host (fedora workstation 30 - localhost)

* Guest1 (fedora server 30 - S1fedoralab - 192.168.100.10)

* Guest2 (fedora server 30 - labserver  - 192.168.100.20) 

* Outils :  *libvirt* , *virt-manager* et accès ssh sécurisé par clés (ed25519). 

--- ---


### SOMMAIRE :

[1. Mettre en place une architecture :](#part1)

* A - Installation des outils de virtualisation sur le Host.
* B - Création du réseau virtuel privé.
* C - Mise en place du ssh sur les deux Guests.

[2. Faire une image de la partition */dev/fedora/backup* au sein de la Guest2 (labserver 192.168.100.20) avec *dd* et la compresser.](#part2)

[3. Transférer de façon sécurisée (paire de clés ed25519) via *sftp* l'image sur la Guest1 (S1fedoralab 192.168.100.10).](#part2)

[4. Distribuer le fichier .xz en *ftp* public vers le Host via notre réseau isolé sur *virbr1* 192.168.100.1/24.](#part4)

[5. Sécuriser le serveur FTP avec certificats en TLS 1.3.](#part5)

[6. Vérifier l’intégrité de la solution de sauvegarde et mettre en place une routine.](#part6)

--- ---

L'intitulé de l’exercice ne précise pas si notre sauvegarde doit être incrémentielle ou pas . Cette notion ne sera pas abordée. 

Aucune notion de temps, de datation.

Aucune prérogative sur les outils à utiliser.

### Nous utiliserons les outils suivants:

* Outils : *libvirt,dd,lz4c,ssh,sftp,vsftpd,openssl*


---

<a name="part1">

### Étape 1 Mettre en place une architecture : 

</a>

  * Remarque : On part du principe que l’interface graphique fournie par *virt-manager* est on ne peut plus pratique. 
Afin de monter un laboratoire sur une workstation cette interface fait gagner beaucoup de temps. 
Il serait contre-productif voire suicidaire de se taper les commandes à la main avec *qemu* en cli.
Par ailleurs les configurations des Guests (fichiers .xml) sont facilement exportables. 
De cette façon on peut par la suite lancer nos Guests en production sur un serveur sans interface graphique avec ces précieux fichiers.

Pour de plus amples informations sur *Libvirt* consultez les [notes Libvirt](https://gitlab.com/valentingrebert/guides/tree/master/Libvirt). 
   
#### A - Installation des outils de virtualisation sur le Host.


```shell
dnf install @virtualization
usermod -a -G libvirt,kvm user
systemctl enable --now libvirtd
```

L'installation des Fedora Server 30 sur les Guests est assez simple à effectuer grâce à l'interface graphique d'installation *anaconda*. Elle ne sera pas abordée.

Nous mettons en place un réseau virtuel privé dans les connexions de *libvirt*.

Le réseau virtuel permet de créer un environnement de travail cloisonné. Nous choisissons cette structure afin de distribuer nos services/fichiers pour le moment car il s'agit d'un laboratoire. 
Nous allons créer un réseau virtuel isolé qui sera bridgé sur l'interface *virbr1* . 192.168.100.1/24

Cette étape peut être réalisée en cli avec *visrh net-edit* ou en gui avec l'interface de *virt-manager*.

Bien entendu nous utilisons au préalable le NAT bridgé sur *virbr0* afin de mettre à jour les systèmes des Guests et d'installer les paquets nécessaires. 

#### B - Création du réseau virtuel privé.  

Dans le gui de *virt-manager* on crée le réseau virtuel c'est très facile. L'on peut également le faire en modifiant le fichier `/etc/libvirt/qemu/networks/default.xml` .

Cependant en cli avec virsh net-edit c'est plus sûr car il verifie la syntaxe et les erreurs.

```shell
virsh net-edit isolated
```

```shell
<network>
  <name>isolated</name>
  <uuid>8f3b17dd-fae5-44cf-9786-d7e08c150d87</uuid>
  <bridge name='virbr1' stp='on' delay='0'/>
  <mac address='52:54:00:1c:45:5b'/>
  <domain name='isolated'/>
  <ip address='192.168.100.1' netmask='255.255.255.0'>
  </ip>
</network>
```

Dans cet exemple nous avons decidé de ne pas utiliser le serveur dhcp integré à libvirt (*dnsmasq*) afin de parametrer à la main les IP de nos VM. 

Si nous voulons utiliser le DHCP interne à *libvirt* voici un exemple de configuration dans les balises IP du fichier.

```shell
  <ip address='192.168.100.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.122.2' end='192.168.122.254'/>
      <host mac='52:54:00:04:0b:f1' name='S1fedoralab' ip='192.168.100.10'/>
      <host mac='52:54:00:72:ca:a9' name='labserver' ip='192.168.100.20'/>
    </dhcp>
  </ip>
```



#### C - Mise en place du ssh sur les deux Guests: 

  * Remarque : D'un point de vue de sysadmin l'on doit quasi systématiquement rendre disponible un serveur via une connexion ssh.
  Nous utilisons *openssh-server* en le sécurisant par paire de clés *ed25519*. 


Cette étape est assez simple à mettre en place. 

_Sur le Host_ :

```shell
ssh-keygen -t ed25519 -f .ssh/id_S1_fedoralab
ssh-copy-id -i .ssh/id_S1_fedoralab user@S1fedoralab
ssh -i .ssh/id_S1_fedoralab user@S1fedoralab
```
_Sur le Serveur_ : Configurez votre fichier `/etc/ssh/sshd_config`.

Pour de plus amples details sur la sécurisation de *ssh* consultez les [notes](https://gitlab.com/valentingrebert/guides/wikis/Configurer-le-serveur-SSH).

<a name="part2">

### Étape - 2 et 3 en même temps. 

</a>

 * Remarque : Deux notions importantes entrent en considération dans la sauvegarde. 
Premièrement nous n'imageons pas un système en utilisation. 
En effet les modifications sur le système de fichiers monté et en cours d'utilisation tel un système (/) risque de nuire à l’intégrité de notre fichier image 
et il risque donc d’être corrompu ce qui annule le principe même de la sauvegarde.
Secondement il faudra proceder à une restauration de l'image afin d'être sur que le processus de sauvegarde est complet.

Nous imagerons une partition qui n'est pas utilisée actuellement par un système.
Dans cet exemple la partition */dev/fedora/backup*.

Secondement nous ne disposons pas sur le Guest *labserver* d'un espace nécessaire pour stocker le fichier image de notre partition. 
Nous le transférerons directement au travers du réseau en utilisant astucieusement *dd*, *xz*, *ssh* et le *pipe*.

* Utilisation des pipes.

Afin de réduire au maximum les commandes nous utilisons la merveilleuse chose qu'est le pipe. 

En effet avec deux pipes et les commandes qui vont bien l'on peut effectuer les étapes 2 et 3 en un seul coup. 
C'est un peu sale mais *dd* est un outil puissant qui permet de copier bit par bit une partition ou un disque. 

```shell
dd if=/dev/fedora/backup conv=notrunc status=progress | lz4c -9 | ssh -i .ssh/id_S1fedoralab root@S1_fedoralab dd of=/var/ftp/pub/labserver.xz
```

Nous précisons qu'aucunes règles dans le temps de la sauvegarde, ni de limite de bande passante ne sont précisées dans les consignes.

Si tel avait été le cas nous aurions pu limiter la bande passante avec *rsync --bwlimit=VALEUR* et ne pas utiliser d'outil de compression.

Nous prenons comme input dans *dd* notre partition en évitant les erreurs avec *conv=notrunc*.

Nous la compressons avec *lz4c -9* qui utilise le *lzma* niveau *-9*.

Nous nous connectons en *ssh* et redirigeons l'output de *dd of=* vers un fichier *.xz* directement à la racine de notre serveur FTP Public.

Plus tard afin de restaurer la sauvegarde et de verifier son bon fonctionnement nous devrons tester la réecriture de l'image:


<a name="part4">

### Étape 4 - Distribuer le fichier .xz en *ftp* public vers le Host

</a>

Sur la machine *S1fedoralab* : 

```shell
dnf install vsftpd
```

```shell
vim /etc/vsftpd.conf
```

```shell
# On active l'option FTP public : 
# Allow anonymous FTP? (Beware - allowed by default if you comment this out).
anonymous_enable=YES
```

On active le service *vsftpd* et on autorise le pare-feu à laisser transiter les paquets.

```shell
systemctl enable --now vsftpd
firewall-cmd --permanent --add-service=ftp
firewall-cmd --reload
```

Et c'est un bingo ! On peut accéder au backup ultra compressé en lzma sur le serveur ftp public.



<a name="part5">

### Étape 5 - Sécuriser le serveur FTP avec certificats en TLS 1.3.

</a>


Nous allons creer nos certificats afin de les configurer dans le serveur.

```shell
mkdir /etc/pki/vsftpd
openssl req -new -newkey rsa:4096 -x509 -sha512 -days 365 -nodes -out /etc/pki/vsftpd/vsftpd.crt -keyout /etc/pki/vsftpd/vsftpd.key
```
Nous remplissons les champs pour generer un certificat en rapport avec notre structure.

Ensuite nous allons configurer *vsftpd* : 

`/etc/vsftpd/vsftpd.conf`

```shell


# Activer le TLS

rsa_cert_file=/etc/pki/vsftpd/vsftpd.crt
rsa_private_key_file=/etc/pki/vsftpd/vsftpd.key
ssl_enable=YES
force_local_data_ssl=YES
force_local_logins_ssl=YES 
allow_anon_ssl=NO

# =Desormais par defaut le TLSv1 est activé. Ces options ne sont plus necessaires.
#ssl_tlsv1=YES
#ssl_sslv2=NO
#ssl_sslv3=NO

# On ajoute l'autorisation pour les ports secure de vsftpd

# fix PASV ports

pasv_enable=YES
pasv_min_port=21000
pasv_max_port=21010
```

On autorise le traffic sur les ports via firewalld.

```shell
firewall-cmd --add-port=21000-21010/tcp --permanent 
firewall-cmd --reload
```


<a name="part6">

### Etape 6 - Vérifier l’intégrité de la solution de sauvegarde et mettre en place une routine.

</a>

On demonte la partition sur *S1_fedoralab* :

```shell
umount /dev/fedora/backup
```


De retour sur notre Host : 

```shell
dd if=/var/ftp/pub/labserver.xz conv=notrunc status=progress | lz4 -d | ssh -i .ssh/id_S1fedoralab root@S1_fedoralab dd of=/dev/fedora/backup
```

* Remarque:  Precisons que l'accès se fait par `root@` sans cela nous ne pourrions ecrire sur `/dev/fedora/backup` . 
Les bonnes pratiques de ssh nous poussent à désactiver le *root authentication*. Ici on le laisse juste car ça fait classe de tout faire en une commande.
Vous pouvez et devez trouver un moyen plus propre d'effectuer cette copie en utilsiant les ACL ou les jails par exemple.

