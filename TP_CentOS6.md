# Serveur Web  , Reverse Proxy , et Sauvegarde sur CentOS6.


Ce tp a pour but de mettre en place:

*Remarque: Pourquoi CentOS 6 ? Pour sortir le candidat de sa "zone de confort" et le preparer à bosser dans des boites ou les OS ne sont pas récentes.

1. Un serveur web *lighttpd* en ecoute locale : http://127.0.0.1:8080:srv/data/

2. Un serveur web *apache* *httpd* qui distribue le site en reverse proxy sur le LAN:  https://192.168.122.1/24:443/portal

3. Un serveur sftp avec utilisateur portal.sftp jailé au repertoire /srv/data et dont les uploads sont automatiquement avec les ACL lighttpd:www-data

4. Un script bash de sauvegarde qui tar le contenu de /srv/data dans /backup chaque jour de la semaine avec 7 sauvegardes max.



## [SOLUTION](https://gitlab.com/valentingrebert/tp_et_td/-/blob/master/TP_CentOS6_Solution.md)