# Mise en place d'un serveur Gitlab et d'un serveur de sauvegarde.

![gitlab-logo](/uploads/dc3b8c9d6aced4ec1a743cab5fcd89b7/gitlab-logo.png)


## Enoncé du TD : 



---

### Installation d'un serveur Gitlab.

1. Vous devrez utiliser deux machines virtuelles au sein d'un hyperviseur (Systeme GNU/Linux obligatoire).

2. Votre serveur Gitlab devra disposer d'une interface Web et de clés SSH pour du commit/push/pull en cli.

3. Vous devrez mettre en place un systeme de sauvegarde securisé et verifier son fonctionnement.

4. Vous devrez servir les sauvegardes de gitlab via un serveur *HTTP* et *FTP* de votre choix.

5. Vous devrez presenter sommairement la technologie gitlab et énoncer votre procedure.

6. BONUS: Mettre en place une node du serveur gitlab sur une 3ieme VM.

---


## [SOLUTION](https://gitlab.com/valentingrebert/tp_et_td/-/blob/master/TP_Gitlab_Solution.md)