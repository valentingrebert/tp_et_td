---

État : En construction

---

# Mise en place d'un serveur Gitlab et d'un serveur de sauvegarde .


![gitlab-logo](/uploads/dc3b8c9d6aced4ec1a743cab5fcd89b7/gitlab-logo.png)




## Enoncé du TD : 

---

### Installation d'un serveur Gitlab.

1. Vous devrez utiliser deux machines virtuelles au sein d'un hyperviseur (Systeme GNU/Linux obligatoire).

2. Votre serveur Gitlab devra disposer d'une interface Web et de clés SSH pour du commit/push/pull en cli.

3. Vous devrez mettre en place un systeme de sauvegarde securisé et verifier son fonctionnement.

4. Vous devrez servir les sauvegardes de gitlab via un serveur *HTTP* et *FTP* de votre choix.

5. Vous devrez presenter sommairement la technologie gitlab et énoncer votre procedure.

6. BONUS: Vous Securiserez vos services et metterez en place une node du serveur gitlab sur une 3ieme VM.

---



## __Présentation de la solution, des outils et de la structure.__ 

Nous proposons l'installation d'une instance [gitlab](https://docs.gitlab.com/) avec la distribution [CentOS 7](https://www.centos.org/download/) sur une VM1. 

Cette distribution est adaptée au monde de l'entreprise par ailleurs la documentation officielle de gitlab propose 
*CentOS 7* et nous n'avons à ce jour effectué aucun test avec *CentOS 8* ou *CentOS Stream*.

Nous mettons en place une VM2 avec la distribution CentOS 7 afin de procéder à des sauvegardes de la VM1 et de distribuer deux services : *http* et *ftp*

Nous automatiserons la sauvegarde avec le client *rsync* à partir de la VM2 en utilisant le protocole *sftp* *:22* sécurisé par une paire de clés *ed25519*.

Nous prévoyons une rotation des sauvegardes sur une semaine qui seront datées et compressées au format *lzma* niveau 9 avec *lz4* afin de minimiser les besoins de stockage.


Voici une presentation sommaire de la solution gitlab. [Gitproject_prez.pdf](/uploads/85b2b672472120d49d546d5bff0ae3ca/Gitproject_prez.pdf)

--- ---

---

* HOST : Système :  *Fedora 30 Workstation*  -   Nom : *localfed* -  IP : 127.0.0.1
* VM1 : Système :  *CENTOS 7* Nom : *S1GITPROJECT*  -  IP : 192.168.100.10/24
* VM2 : Système :  *CENTOS 7* Nom : *S1BACKUP*  -  IP : 192.168.100.20/24
* Virtualisation :  *Libvirt (kvm/qemu), virt-manager.*
* Réseau virtuel sur *virbr1* 192.168.100.1/24 .

---

--- ---


## Sommaire

[1 - Installation du système *CentOS 7* sur la VM1 et la VM2.](#part1)
* A - Installation de la VM1 
    Partitionnement
    Premières opération à effectuer
* B - Installation de la VM2
    
[2 - Configuration du *SSH* entre le host et la VM1 et VM2.](#part2)

[3 - Installation et configuration du serveur *Gitlab*.](#part3)

[4 - Création des utilisateurs et vérification du fonctionnement du serveur.](#part4)

[5 - Installation de rsync et mise en place de la sauvegarde.](#part5)

[6 - Installation de NGINX, VSFTPD et mise en place de l'*index.html* sur la VM2 S1BACKUP ](#part6)


--- ---

<a name="part1">

## 1 - Installation du système *CentOS 7* sur la VM1 et la VM2.

</a>


Nous ne documentons pas l'installation de CentOS sur les VM car l'interface graphique *anaconda* est très simple à utiliser. 

Si vous ne parvenez pas à installer CentOS de façon graphique veillez soigneusement à changer de profession.


 * Remarque: Nous proposons notre propre partitionnement pour nos deux VM. Libre à vous de faire vos propres partitionnements.
Il faut garder à l'esprit que la plan de partitionnement est extrêmement important. 
Au sein d'une infrastructure distribuée nous ne nous posons pas les mêmes questions mais pour un environnement tel que le notre il faut veiller à un plan cohérent. 
Pour de plus amples informations référez-vous aux pratiques recommandées par [Red Hat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_a_standard_rhel_installation/partitioning-reference_installing-rhel#recommended-partitioning-scheme_partitioning-reference) 


Le système de partitionnement est LVM avec allocation fine.

 * Remarque :
Nous utilisons le [lvm thin provisionning](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/logical_volume_manager_administration/lv_overview#thinprovisioned_volumes). 
Cette technologie permet d'adapter la taille de nos *logical volumes* de façon dynamique. 
Elle est très pratique dans l'optique des clusters et de la haute disponibilité *HA* (des éléments du principe de *scalability*). 


### A - Partitionnement :

```shell
lvs
```

```shell
  LV     VG              Attr       LSize   Pool   Origin Data%  Meta%  Move Log Cpy%Sync Convert
  pool00 cs_s1gitproject twi-aotz-- <29,98g               15,35  30,27                           
  root   cs_s1gitproject Vwi-aotz-- <19,98g pool00        19,58                                  
  swap   cs_s1gitproject -wi-ao----   1,60g                                                      
  var    cs_s1gitproject Vwi-aotz--  10,00g pool00        6,90      
  ```


### B - Premières opérations à effectuer :

En premier lieu, il faut vérifier :

* L'état du pare-feu et sa mise en place.
* L'état des mises à jour et l'upgrade du système.

L'état et l'activation du pare-feu avec *firewalld* .

```shell
systemctl status firewalld
systemctl enable --now firewalld

```

Les mises à jour avec le puissant outil *yum* .

```shell
yum update

```

* Remarque: Afin de mesurer dans le temps la période de travail et de dater les opérations nous ajoutons une fonctionnalité dans les variables d'environnement
qui va nous permettre de dater les commandes entrées dans le terminal :

En root :

```shell
echo 'export HISTTIMEFORMAT="%d/%m/%y %T "' >> ~/.bash_profile
 . ~/.bash_profile
```

En simple user : 

```shell
$ echo 'export HISTTIMEFORMAT="%d/%m/%y %T "' >> ~/.bash_profile
$ . ~/.bash_profile
```

Ainsi lorsque nous entrerons la commande `history` nous pourrons voir les heures auxquelles les commandes ont été entrées.



### B - Installation de la VM2 :


L'installation de notre deuxième VM suit la même procédure que l'installation de la VM1.

Cependant, nous proposons un partitionnement différent :

```shell
lvs
```

```shell
  LV         VG     Attr       LSize  Pool   Origin Data%  Meta%  Move Log Cpy%Sync Convert
  mnt_backup centos Vwi-aotz-- 10,00g pool00        0,11                                   
  pool00     centos twi-aotz-- 34,96g               4,20   15,33                           
  root       centos Vwi-aotz-- 24,96g pool00        5,83                                   
  swap       centos -wi-ao---- <1,61g 
```


* ATTENTION : Veillez à installer *rsync* sur les VM1 et VM2 afin de pouvoir réaliser les backup en toute serenité.



--- ---


<a name="part2">

### 2 - Configuration du *SSH* entre le host, la VM1 et la VM2.

</a>


 * Remarque: Il s'agit d'une configuration sommaire. Si vous voulez de plus amples informations sur SSH son fonctionnement et la sécurité
suivez [ces notes](https://gitlab.com/valentingrebert/guides/wikis/ssh).



On configure le serveur pour accepter les clés publiques (ed25519) et l'authentification par mot de passe (dans un premier temps.)

La configuration sur les deux VM est la même. 


1. On édite `/etc/ssh/sshd_config` sur le serveur : 


```bash
# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 22
AddressFamily any
ListenAddress 0.0.0.0
ListenAddress ::

#HostKey /etc/ssh/ssh_host_rsa_key
#HostKey /etc/ssh/ssh_host_dsa_key
#HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

# Ciphers and keying
#RekeyLimit default none

# Logging
#SyslogFacility AUTH
SyslogFacility AUTHPRIV
#LogLevel INFO

# Authentication:

LoginGraceTime 2m
PermitRootLogin no
StrictModes yes
MaxAuthTries 6
MaxSessions 10

PubkeyAuthentication yes

# The default is to check both .ssh/authorized_keys and .ssh/authorized_keys2
# but this is overridden so installations will only check .ssh/authorized_keys
AuthorizedKeysFile	.ssh/authorized_keys

#AuthorizedPrincipalsFile none

#AuthorizedKeysCommand none
#AuthorizedKeysCommandUser nobody

# For this to work you will also need host keys in /etc/ssh/ssh_known_hosts
#HostbasedAuthentication no
# Change to yes if you don't trust ~/.ssh/known_hosts for
# HostbasedAuthentication
#IgnoreUserKnownHosts no
# Don't read the user's ~/.rhosts and ~/.shosts files
#IgnoreRhosts yes

# To disable tunneled clear text passwords, change to no here!
#PasswordAuthentication yes
#PermitEmptyPasswords no
PasswordAuthentication yes

# Change to no to disable s/key passwords
#ChallengeResponseAuthentication yes
ChallengeResponseAuthentication no

# Kerberos options
#KerberosAuthentication no
#KerberosOrLocalPasswd yes
#KerberosTicketCleanup yes
#KerberosGetAFSToken no
#KerberosUseKuserok yes

# GSSAPI options
GSSAPIAuthentication yes
GSSAPICleanupCredentials no
#GSSAPIStrictAcceptorCheck yes
#GSSAPIKeyExchange no
#GSSAPIEnablek5users no

# Set this to 'yes' to enable PAM authentication, account processing,
# and session processing. If this is enabled, PAM authentication will
# be allowed through the ChallengeResponseAuthentication and
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via ChallengeResponseAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# If you just want the PAM account and session checks to run without
# PAM authentication, then enable this but set PasswordAuthentication
# and ChallengeResponseAuthentication to 'no'.
# WARNING: 'UsePAM no' is not supported in Red Hat Enterprise Linux and may cause several
# problems.
UsePAM yes

#AllowAgentForwarding yes
#AllowTcpForwarding yes
#GatewayPorts no
X11Forwarding yes
#X11DisplayOffset 10
#X11UseLocalhost yes
#PermitTTY yes
#PrintMotd yes
#PrintLastLog yes
#TCPKeepAlive yes
#UseLogin no
#UsePrivilegeSeparation sandbox
#PermitUserEnvironment no
#Compression delayed
#ClientAliveInterval 0
#ClientAliveCountMax 3
#ShowPatchLevel no
#UseDNS yes
#PidFile /var/run/sshd.pid
#MaxStartups 10:30:100
#PermitTunnel no
#ChrootDirectory none
#VersionAddendum none

# no default banner path
#Banner none

# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS

# override default of no subsystems
Subsystem	sftp	/usr/libexec/openssh/sftp-server

# Example of overriding settings on a per-user basis
#Match User anoncvs
#	X11Forwarding no
#	AllowTcpForwarding no
#	PermitTTY no
#	ForceCommand cvs server
```

On vérifie l'état du service *sshd* et on l'active au démarrage et immédiatement grâce à l'option ```enable --now```
s'il est deja lancé nous le relançons avec : ```systemctl restart sshd```

```shell
systemctl status sshd
systemctl enable --now sshd
```

Il faut ensuite créer une règle afin d'autoriser le trafic sur le port :22 pour le service *sshd*.

Nous créons cette règle __après__ avoir configuré le *ssh* correctement, pas __avant__ :

```shell
firewall-cmd --permanent --add-service=ssh
firewall-cmd --reload
```

* Sur le Host :

À présent nous allons configurer la paire de clés afin d'accéder à nos VM à partir d'un terminal sur le Host.

Notez que nous avons créé un utilisateur *user1* pendant l'installation du système afin de ne pas laisser *root* se connecter en ssh.

Création des clés :

```shell
ssh-keygen -t ed25519 -f .ssh/S1_GITPROJECT
ssh-copy-id -i .ssh/S1_GITPROJECT.pub user1@192.168.100.10
ssh -i .ssh/S1_GITPROJECT user1@192.168.100.10
```


Une fois le test de connectivité effectué et qu'il est probant nous reconfigurons *sshd* sur les deux VM afin d'interdire l'authentification par mot de passe.

* Sur les deux VM : 

```shell
vim /etc/ssh/sshd_config
```

Et nous mettons la valeur *no* sur cette ligne: 

```shell
PasswordAuthentication no
```


Nous redémarrons le service :

```shell
systemctl restart sshd
```


Nous testons à nouveau la connectivité à partir du Host. 


Notre système sur la VM1 est à jour et presque prêt à recevoir son serveur *Gitlab*.


* ATTENTION !!!
Afin de nous prémunir d'éventuels problèmes durant l'installation et la configuration nous effectuons un SNAPSHOT de nos VM.


--- ---

<a name="part3">

## 3 - Installation du serveur *Gitlab* sur la VM1.

</a>

* Remarque: 
Nous avons rencontré des problèmes avec la configuration ssl initiée par le script de type: 
`cannot load certificate "/etc/gitlab/ssl/`
En configurant `/etc/hostname` et `/etc/hosts` avec la valeur `s1gitproject.localdomain` nous ne rencontrons plus ces problèmes.



Nous installons les outils nécessaires et autorisons le traffic sur le port 80 (http) (< temporairement) et 443 (https) .

```shell
yum install -y curl policycoreutils-python lz4 rsync perl postgresql-contrib postfix
firewall-cmd --permanent --add-service=http
firewall-cmd --permanent --add-service=https
firewall-cmd --reload
```

Ce n'est pas précisé dans la documentation officielle mais nous avons du ajouter le paquet : 

```shell
perl
```

La prochaine commande permet de télécharger un script afin de modifier vos repositories .

* ATTENTION : Cette étape est importante mais doit être effectuée avec précaution.
Par ailleurs n’exécutez jamais en root un script trouvé sur le net sans l'avoir au préalable lu et compris.

Ce script installe de multiples outils du [bundle](https://docs.gitlab.com/ee/development/architecture.html#component-list) de gitlab.

Entre autres : __Nginx, PostgreSQL, Redis, Sidekiq, Grafana, Prometheus, Logrotate , Unicorn , Gitlab , AlertManager.__

### Voici la structure de ces outils.  

![architecture_simplified](/uploads/71e7b0cd97fa2ca7eacf03318019258a/architecture_simplified.png)



## Nous commençons l'installation. 

```shell
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | bash
```

On remplace *https://gitlab.example.com* par l'addresse de notre serveur git :

```shell
# EXTERNAL_URL="https://s1gitproject.localdomain" yum install -y gitlab-ee
```


Afin de modifier la configuration de gitlab nous modifions le fichier :

```shell
vim /etc/gitlab/gitlab.rb
```

Après modification nous reconfigurons gitlab avec la commande :

```shell
gitlab-ctl reconfigure
```

* Attention: 
* Afin de faire fonctionner correctement les certificats auto-signés crées par le script appliquez les permissions suivantes:
* `chmod 0700 /etc/gitlab/ssl`


Une fois l'installation effectuée on peut vérifier l'état du service de gitlab qui englobe les instances des autres outils.

```shell
gitlab-ctl status
```

Ce qui retourne: 

```shell
run: alertmanager: (pid 1513) 14202s; run: log: (pid 1511) 14202s
run: gitaly: (pid 1533) 14202s; run: log: (pid 1532) 14202s
run: gitlab-exporter: (pid 1528) 14202s; run: log: (pid 1525) 14202s
run: gitlab-workhorse: (pid 1519) 14202s; run: log: (pid 1512) 14202s
run: grafana: (pid 1515) 14202s; run: log: (pid 1514) 14202s
run: logrotate: (pid 31587) 3402s; run: log: (pid 1523) 14202s
run: nginx: (pid 1535) 14202s; run: log: (pid 1534) 14202s
run: node-exporter: (pid 1541) 14202s; run: log: (pid 1538) 14202s
run: postgres-exporter: (pid 1530) 14202s; run: log: (pid 1529) 14202s
run: postgresql: (pid 1518) 14202s; run: log: (pid 1516) 14202s
run: prometheus: (pid 1522) 14202s; run: log: (pid 1517) 14202s
run: redis: (pid 1527) 14202s; run: log: (pid 1524) 14202s
run: redis-exporter: (pid 1539) 14202s; run: log: (pid 1536) 14202s
run: sidekiq: (pid 1540) 14202s; run: log: (pid 1537) 14202s
run: unicorn: (pid 1521) 14202s; run: log: (pid 1520) 14202s
```

Si nous voulons connaître la version des services utilisés par le bundle de gitlab ce fichier est très intéressant: 

```shell
less /opt/gitlab/version-manifest.txt
```

Au niveau du réseau nous pouvons vérifier la présence et l'écoute des sockets nécessaires avec cette puissante commande: 

```shell
ss -lapute
```


Il faut bien entendu comprendre la structure des [logs de gitlab](https://docs.gitlab.com/omnibus/settings/logs.html) et ses sous instances. 

Pour le moment nous loggons gitlab de manière générale avec cette commande : 

```shell
gitlab-ctl tail
```

La fonction de log est aussi intégrée à l'interface web : 

(https://192.168.100.10/admin/logs)

<a name="part4">

## 4 - Création des utilisateurs et vérification du fonctionnement du serveur.

</a>

Cette étape à été effectuée grâce à l'interface Web de gitlab. 

En effet lors de la première connexion sur un navigateur web à l'adresse > https://192.168.100.10
nous sommes redirigés vers l'interface de connexion de gitlab qui nous pousse dans un premier temps à changer le mot de passe root de l'administrateur du serveur. 

Une fois cette étape effectuée nous procédons à la création de nos utilisateurs directement via l'interface. 

Nous testons le fonctionnement en créant un groupe , des utilisateurs et un projet.

Nous demandons à un client de tester le push, pull et commit sur le projet via l'interface Web et via ssh.

Une fois que nous sommes assurés que notre serveur fonctionne en https sur le port :443 nous interdisons le http :80  

```shell
firewall-cmd --permanent --remove-service=http
firewall-cmd --reload
```


<a name="part5">

## 5 - Installation de *rsync* et mise en place de la sauvegarde interne à gitlab

</a>
 
* Remarque : Nous mettons en place dans cette étape un serveur de backup sur la VM2 *S1BACKUP* au sein du même Host (Hyperviseur). 
  Nous comprenons que cette solution n'est pas une bonne pratique. 
  En effet si pour x raisons l'hyperviseur et/ou son stockage tombent tout le principe de la sauvegarde s'annule. 
  Il faut en effet externaliser le backup en dehors de l'hyperviseur.
  Dans le cadre de ce TP nous n'externalisons pas la sauvegarde nous restons dans un laboratoire de tests.


Gitlab est une plateforme vraiment très puissante elle intègre des [outils de backup](https://docs.gitlab.com/omnibus/settings/backups.html). 

Nous allons les utiliser et les coupler à *rsync* pour procéder à nos sauvegardes.

Par mesures de sécurité nous créons un utilisateur *userbackup* sur la VM1 et nous autorisons à cet utilisateur l'accès au répertoire */var/backups/*

Nous allons séparer les sauvegardes dans deux répertoires :

 A- C'est `/var/backups/git/` qui contiendra l'archive datée des fichiers de configuration de git notamment les clés du chiffrement de la base de données et le fichier de configuration.

```shell
/etc/gitlab/gitlab-secrets.json
/etc/gitlab/gitlab.rb
```

B - C'est `/var/backups/git_base/` qui contiendra l'archive datée de toute l'instance de git : 

* Database
* Attachments
* Git repositories data
* CI/CD job output logs
* CI/CD job artifacts
* LFS objects
* Container Registry images
* GitLab Pages content



Stocker au même endroit les clés de la base de données et cette même base représente un risque.

C'est ainsi pour des raisons de sécurité que *gitlab* propose de séparer la sauvegarde de l'instance entière et celle des configurations.


A- Configuration et clés. 

Nous allons mettre en place la sauvegarde des configurations et clés avec crontab. 

Nous éditons un *crontab* sur la VM1 qui héberge l'instance de Gitlab. 

Nous voulons une sauvegarde le mardi et le vendredi à 16h.
Pour cela nous éditons la règle crontab en root :

```shell
crontab -e
```

```shell
15 04 * * 2,5  gitlab-ctl backup-etc && cd /etc/gitlab/config_backup && cp $(ls -t | head -n1) /var/backups/git/ && chown -R root:userbackup /var/backups && chmod -R g+r,u+rwx /var/backups
```

Cette commande sauvegarde et archive les données dans le répertoire ```/etc/gitlab/``` . Elle confère aussi à l'utilisateur *userbackup* les droits sur le répertoire */var/backups* .
Il est judicieux de la tester avant de la placer dans le cron. 


B - L’instance de git.

Nous précisons que nous voulons sauvegarder et archiver la sauvegarde dans ```/var/backups/git_base/```

Pour des raisons de sécurité nous allons chiffrer la sauvegarde de git grâce aux options présentes dans ```/etc/gitlab/gitlab.rb```

Ces réglages se trouvent dans la zone : ```### Backup Settings```

* Remarque : L'outil de sauvegarde de gitlab propose de sauvegarder notre instance directement sur un serveur ou stockage distant.
Dans le cadre de ce projet nous utilisons rsync à des fins didactiques. 

Nous précisons le répertoire ou nous voulons placer nos backups et la périodicité est d'une semaine.

```shell
gitlab_rails['backup_path'] = "/var/backups/git_base/"
gitlab_rails['backup_keep_time'] = 604800

```

Nous testons notre commande avant de la mettre dans un cron.

```shell
gitlab-backup chown -R root:userbackup /var/ftp/ && chmod -R g+rw,u+rwx /var/ftp/
```




Le but d'une sauvegarde est d'être restaurée et de fonctionner correctement. 

Sur la VM2 nous mettons en place un crontab qui va récupérer les sauvegardes de la VM1 (chiffrées) avec rsync : 

```shell
crontab -e
```

```shell
17 17 * * 2,5 chown -R root:userbackup /var/ftp && chmod -R ugo+rwx /var/ftp && rsync -avz -e "ssh -i /home/userbackup/.ssh/S1_GITPROJECT" userbackup@192.168.100.10:/var/backups/git_base/ /var/ftp/pub/
```

Nous allons la restaurer et vérifier que notre serveur gitlab est toujours fonctionnel et correspond à la date du backup restauré.

```shell
gitlab-ctl restore
```


<a name="part6">

## 6 - Installation de *NGINX*, *VSFTPD* et mise en place de l'*index.html* sur la VM2 *S1BACKUP*.

</a>



A - Préparation des certificats. Il serviront également pour le serveur FTP.

```shell
mkdir /etc/pki && cd /etc/pki
```

```shell
openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out gitproject.crt -keyout gitproject.key
```


B - Installation et configuration NGINX sur la VM2 :

On installe nginx:

```shell
yum install nginx
```

On vérifie l'état du service et on l'active au démarrage :

```shell
systemctl status nginx
systemctl enable --now nginx
```

On autorise le trafic sur les ports 80 (*http*) et 443 (*https*) et on reload le firewall :

```shell
firewall-cmd --permanent --add-service=http
firewall-cmd --permanent --add-service=https
firewall-cmd --reload
```

Avant de configurer *NGINX* nous copions les certificats au bon endroit :

```shell
cp /etc/pki/gitproject.* /etc/pki/nginx/ && mv /etc/pki/nginx/gitproject.key /etc/pki/nginx/private/
```

`/etc/nginx/nginx.conf`

```perl
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 4096;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

#    server {add
#       listen       80 default_server;
#        listen       [::]:80 default_server;
#        server_name  GitProject2;
#        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

#        location / {
#        }

#        error_page 404 /404.html;
#            location = /40x.html {
#        }

#        error_page 500 502 503 504 /50x.html;
#            location = /50x.html {
#        }
#    }

# Settings for a TLS enabled server.
#
    server {
        listen       443 ssl http2 default_server;
        listen       [::]:443 ssl http2 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        ssl_certificate "/etc/pki/nginx/gitproject.crt";
        ssl_certificate_key "/etc/pki/nginx/private/gitproject.key";
        ssl_session_cache shared:SSL:1m;
        ssl_session_timeout  10m;
        ssl_ciphers HIGH:!aNULL:!MD5;
        ssl_prefer_server_ciphers on;
    ssl_protocols TLSv1.3;

#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
        location / {
        }
#
        error_page 404 /404.html;
            location = /40x.html {
        }
#
        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }

}
```


Dès que le fichier de configuration *NGINX* est modifié on peut le vérifier avec la commande :

```shell
nginx -t -c /etc/nginx/nginx.conf
```

Puis on le reload :

```shell
systemctl reload nginx
```

Lorsque nous avons vérifié que notre serveur sert bien en https sur le port 443 nous interdisons le http dans les règles du pare-feu :

```shell
firewall-cmd --permanent --remove-service=http
firewall-cmd --reload
```



C - Mise en place de l'*index.html* et crontab :

Les fichiers HTML et CSS se situent dans */usr/share/nginx/html/*.
On édite notre *index.html* afin de lui rajouter un lien vers une page *download.html* dans laquelle on affichera une liste de nos backups disponibles en téléchargement en *FTPS*.

Afin d'actualiser automatiquement la liste des backups disponibles en téléchargement, nous mettons en place un script en bash qui va actualiser le code *html* de notre page *download.html*.
Pour ceci on édite un fichier *download.orig* qui nous servira de code de base :

```html
<!DOCTYPE html>
<html>
    <head>
        <title>GitProject</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" media="screen" type="text/css" title="Exempleresponsive" href="style.css"/>
    </head>
    <body>
        <div class="header">
            <h1 style="font-size:300%">GitProject</h1>
            <p>Bienvenue sur la page d'un super projet</p>
        </div>

        <div class="navbar">
            <a href="index.html">About</a>
            <a href="download.html">Download</a>
        </div>

        <div class="corps">
            <h3>Download :</h3>
            <p>Voici les diférents backups :</p>
            <div>
                <ul>
                <!-- on insère la liste ici -->
                </ul>
            </div>
        </div>

        <div class="footer">
            <p>M2i Formation Administrateur UNIX/Linux<br>Promo Beauvais 2019</p>
        </div>
    </body>
</html>
```

Le script va :
* faire une copie de download.orig en download.html qui sera modifié
* générer la liste des noms des backups à récupérer dans un fichier texte (liste.txt)
* créer un fichier texte qui transpose cette liste en code html qui sera placé entre les balises <ul></ul> (listeindex.txt)
* placer ce code dans son emplacement dans download.html

```bash
!/bin/bash
# vérification du download.htm
if [ -e "download.html" ]
then
    	rm download.html
fi
cp download.orig download.html

# création de liste.txt
ls /var/ftp/pub/ > liste.txt

# variables
chemin="ftp://192.168.100.20/pub/"
max=`awk 'END {print NR}' liste.txt`

# vérification existence listeindex.txt
if [ -e "listeindex.txt" ]
then
    	rm listeindex.txt
fi
touch listeindex.txt

# boucle code
for (( i=1; i<=$max; i++ ))
do
  	name=`sed -n "$i"p liste.txt`
        echo "<li><a href='$chemin'>$name</a></li>" >> listeindex.txt
done

# envoie du code dans download.html
for (( i=1; i<=$max; i++ ))
do
  	ligne=`sed -n "$i"p listeindex.txt`
        sed -i "/<ul>/a $ligne" download.html
done
```

* Crontab pour actualiser download.html :

```shell
crontab -e
```


```shell
20 17 * * 2.5 bash /usr/share/nginx/html/script.sh
```

D - Mise en place d'un serveur FTP public sur la VM2 S1BACKUP.


Cette étape est rapide. Il n'y a pas de mesures de sécurisation du serveur il sera en interne et public.

```shell
yum install vsftpd
```

Nous modifions la configuration pour permettre le ftp public qui ira chercher ses fichiers dans le répertoire */var/ftp/pub/* :

```perl
# Allow anonymous FTP? (Beware - allowed by default if you comment this out).
anonymous_enable=YES
#
# Uncomment this to allow local users to log in.
# When SELinux is enforcing check for SE bool ftp_home_dir
local_enable=NO
#
# Uncomment this to enable any form of FTP write command.
write_enable=NO 
#
# Default umask for local users is 077. You may wish to change this to 022,
# if your users expect that (022 is used by most other ftpd's)
local_umask=022
```

Nous redémarrons notre service et ajoutons une règle FTP dans notre pare-feu.

```shell
firewall-cmd --permanent --add-service=ftp
systemctl restart vsftpd
```

Nous vérifions la connectivité directement dans le navigateur.

### Ressources :

https://about.gitlab.com/install/#centos-7

https://docs.gitlab.com/omnibus/settings/nginx.html#manually-configuring-https

https://docs.gitlab.com/ee/install/requirements.html